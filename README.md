# Preliminary numerical sensitivity analyses on an injection-molded center-gated disk

## Description

The notebook contained in this repository is supposed to accompany our manuscript "Flow-fiber coupled injection molding simulations with non-uniform fiber concentration effects". It presents some numerical sensitivity analyses (mesh, time-step, initial conditions) performed for an injection-molded center-gated disk.

You can visualize the notebook by [opening this link](https://nbviewer.jupyter.org/urls/bitbucket.org/litianyi/center_gated_disk/raw/master/center_gated_disk.ipynb).

## Contact

Dr. Tianyi Li, tianyi.li (at) promold.fr